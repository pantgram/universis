import {Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import * as SEMESTER_LIST_CONFIG from './preview-semester-rules.config.list.json';
import {DIALOG_BUTTONS, ErrorService, ModalService, ToastService} from '@universis/common';
import {Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedTableService} from '@universis/ngx-tables';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';


@Component({
  selector: 'app-preview-semester-rules',
  templateUrl: './preview-semester-rules.component.html',
})

export class PreviewSemesterRulesComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>SEMESTER_LIST_CONFIG;
  @ViewChild('rules') semesterRules: AdvancedTableComponent;
  @Input() searchConfiguration: any;
  @ViewChild('search') search: AdvancedSearchFormComponent;

  studyProgram: any = this._activatedRoute.snapshot.params.id;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;
  private dataSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
  ) {
  }

  async ngOnInit() {

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studyProgram = params.id;
      this._activatedTable.activeTable = this.semesterRules;
      this.semesterRules.query = this._context.model('StudyProgramSemesterRules')
        .where('studyProgram').equal(params.id)
        .expand('courseTypes')
        .orderBy('semester')
        .prepare();

      this.semesterRules.config = AdvancedTableConfiguration.cast(SEMESTER_LIST_CONFIG);
      this.semesterRules.fetch();
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.semesterRules.fetch(true);
        }
      });
      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        if (data.tableConfiguration) {
          this.semesterRules.config = data.tableConfiguration;
          this.semesterRules.ngOnInit();
        }
        if (data.searchConfiguration) {
          this.search.form = data.searchConfiguration;
          this.search.formComponent.formLoad.subscribe((res: any) => {
            Object.assign(res, {studyProgram: this._activatedRoute.snapshot.params.id});
          });
        }
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  remove() {
    if (this.semesterRules && this.semesterRules.selected && this.semesterRules.selected.length) {
      // get items to remove
      const items = this.semesterRules.selected.map(item => {
        return {
          id: item.id
        };
      });
      return this._modalService.showWarningDialog(
        this._translateService.instant('StudyPrograms.RemoveSemesterTitle'),
        this._translateService.instant('StudyPrograms.RemoveSemesterMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
        if (result === 'ok') {
          this._context.model('StudyProgramSemesterRules').remove(items).then(() => {
            this._toastService.show(
              this._translateService.instant('StudyPrograms.RemoveSemestersMessage.title'),
              this._translateService.instant((items.length === 1 ?
                'StudyPrograms.RemoveSemestersMessage.one' : 'StudyPrograms.RemoveSemestersMessage.many')
                , {value: items.length})
            );
            this.semesterRules.fetch(true);
          }).catch(err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });
    }
  }
}
