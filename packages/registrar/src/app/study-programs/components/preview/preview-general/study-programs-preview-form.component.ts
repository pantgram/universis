import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
    selector: 'app-study-programs-preview-form',
    templateUrl: './study-programs-preview-form.component.html'
})
export class StudyProgramsPreviewFormComponent implements OnInit {

    @Input() model: any;

    constructor(private _activatedRoute: ActivatedRoute,
        private _context: AngularDataContext) {
    }

    async ngOnInit() {

    }

}
