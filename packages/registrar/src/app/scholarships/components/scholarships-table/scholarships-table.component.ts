import {Component, Input, OnDestroy, OnInit, Output, EventEmitter, ViewChild} from '@angular/core';
import {AdvancedRowActionComponent, AdvancedTableComponent, AdvancedTableDataResult} from '@universis/ngx-tables';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';

import { Router, ActivatedRoute } from '@angular/router';
import {AdvancedTableSearchComponent} from '@universis/ngx-tables';
import {ActivatedTableService} from '@universis/ngx-tables';
import {UserActivityService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {ClientDataQueryable} from '@themost/client';
import {Observable, Subscription} from 'rxjs';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, LoadingService, ModalService } from '@universis/common';

@Component({
  selector: 'app-scholarships-table',
  templateUrl: './scholarships-table.component.html',
  styleUrls: ['./scholarships-table.component.scss']
})
export class ScholarshipsTableComponent implements OnInit, OnDestroy  {

  private dataSubscription: Subscription;
  private paramSubscription: Subscription;

  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;

  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  private selectedScholarships: any;
  public recordsTotal: any;
  private fromYear: any;

  onSearchKeyDown(event: any) {
    if (event.keyCode === 13) {
      this.table.search((<HTMLInputElement>event.target).value);
      return false;
    }
  }

  constructor(private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _userActivityService: UserActivityService,
              private _translateService: TranslateService,
              private _errorService: ErrorService,
              private _loadingService: LoadingService,
              private _modalService: ModalService,
              private _context: AngularDataContext) {
  }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        // set config
        this.table.config = data.tableConfiguration;
        // reset search text
        this.advancedSearch.text = null;
        // reset table
        this.table.reset(false);
      }

      this._userActivityService.setItem({
        category: this._translateService.instant('Scholarships.Title'),
        description: this._translateService.instant(this.table.config.title),
        url: window.location.hash.substring(1), // get the path after the hash
        dateCreated: new Date
      });
    });
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id','totalStudents'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected;
        }
      }
    }
    return items;
  }


  // Deletes selected Scholarships
  async deleteScholarshipAction() {
    try{
      this._loadingService.showLoading();
      const scholarships = await this.getSelectedItems();
      this.selectedScholarships = scholarships.filter(scholarship => {
        return scholarship.totalStudents === 0 || scholarship.totalStudents === null;
      });      

      this._modalService.openModalComponent(AdvancedRowActionComponent,{
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedScholarships,
          modalTitle: "Scholarships.DeleteScholarshipAction.Title",
          description: "Scholarships.DeleteScholarshipAction.Description",
          errorMessage: "Scholarships.DeleteScholarshipAction.CompletedwithErrors",
          refresh: this.refreshAction,
          execute: this.executeDeleteScholarshipAction()
        }

      })

    } catch(err) { 
      this._errorService.showError(err, {
        continueLink: '.'
      });

    } finally {
      this._loadingService.hideLoading();
    }
  } 

  // Execute the delete scholarship action 
  executeDeleteScholarshipAction() {
    // Return an Observable that performs the delete action
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1 // Emit a progress update with initial progress value
      });
      const result = {
        total: this.selectedScholarships.length,
        success: 0,
        errors: 0
      };
      // execute update for all items (transactional update)
      // map items
      const updated = this.selectedScholarships.map((item) => {
        return {
          id: item.id
        };
      });
      // Simulate progress with intervals
      let progressValue = 5;
      const progressInterval = setInterval(() => {
        progressValue = progressValue + 10 < 100 ? progressValue + 10 : 5;
        this.refreshAction.emit({
          progress: progressValue
        });
      }, 1000);
      this._context.model('Scholarships').remove(updated).then(() => {
        // After successful deletion, reload the table
        this.table.fetch(true);
        // Stop the progress interval
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result
        return observer.next(result);
      }).catch((err) => {
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result with errors
        result.errors = result.total;
        return observer.next(result);
      });
    });
  }


  async copyScholarshipAction() {
    try {
      this._loadingService.showLoading();
      const scholarships = await this.getSelectedItems();
      let fromYear =  [...scholarships.map(item => item.yearAlternateName
        )];
      fromYear = fromYear.filter( function( item, index, inputArray ) {
        return inputArray.indexOf(item) === index;
      });

      if (fromYear.length !== 1) {
        this.selectedScholarships = [];
      } else {
        this.selectedScholarships = scholarships;
        this.fromYear = fromYear;
      }

      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedScholarships,
          formTemplate: this.selectedScholarships && this.selectedScholarships.length > 0 ? 'Scholarships/copy' : null,
          modalTitle: 'Scholarships.CopyScholarshipAction.Title',
          description: 'Scholarships.CopyScholarshipAction.Description',
          errorMessage: 'Scholarships.CopyScholarshipAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeCopyAction()
        }
      });
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    } finally {
      this._loadingService.hideLoading();
    }

  }


    /**
   * Executes copy action for course classes
   */
    executeCopyAction() {
      return new Observable((observer) => {
        const total = this.selectedScholarships.length;
        const result = {
          total: this.selectedScholarships.length,
          success: 0,
          errors: 0
        };
        // get values from modal component
        const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
        const data = component.formComponent.form.formio.data;
        if (data.year == null || data.year === '') {
          this.selectedScholarships = [];
          result.errors = result.total;
          return observer.next(result);
        }
        this.refreshAction.emit({
          progress: 1
        });
        // execute promises in series within an async method
        (async () => {
          for (let index = 0; index < this.selectedScholarships.length; index++) {
            try {
              const item = this.selectedScholarships[index];
              // set progress
              this.refreshAction.emit({
                progress: Math.floor(((index + 1) / total) * 100)
              });

                await this._context.model(`Scholarships/${encodeURIComponent(item.id)}/copy`).save(data);
                result.success += 1;

              // do not throw error while updating row
              // (user may refresh view)
            } catch (err) {
              // log error
              console.log(err);
              result.errors += 1;
            }
          }
          try {
            // Fetch the updated table
            this.table.fetch(true);
          } catch (err) {
            console.log(err);
          }
        })().then(() => {
          observer.next(result);
        }).catch((err) => {
          observer.error(err);
        });
      });
    }

}
