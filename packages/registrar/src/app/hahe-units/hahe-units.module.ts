import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './components/list/list.component';
import { ElementsModule } from '../elements/elements.module';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ErrorModule, SharedModule } from '@universis/common';
import { TablesModule } from '@universis/ngx-tables';
import { RouterModule, Routes } from '@angular/router';
import { MostModule } from '@themost/angular';
import { RouterModalModule } from '@universis/common/routing';
import { NgArrayPipesModule } from 'ngx-pipes';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { HaheUnitsSharedModule } from './hahe-units-shared.module';
import {DepartmentsListComponent} from './components/departments/list.component';
import {AdvancedFormsModule} from '@universis/forms';
import {StudyProgramsListComponent} from './components/study-programs/list.component';


const routes: Routes = [
  {
    path: 'list',
    component: ListComponent
  },
  {
    path: 'departments',
    component: DepartmentsListComponent
  },
  {
    path: 'studyPrograms',
    component: StudyProgramsListComponent
  }
];

@NgModule({
  imports: [
    ElementsModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    TranslateModule,
    TablesModule,
    SharedModule,
    ErrorModule,
    MostModule,
    AdvancedFormsModule,
    RouterModalModule,
    NgArrayPipesModule,
    NgxDropzoneModule,
    HaheUnitsSharedModule
  ],
  declarations: [
    ListComponent,
    DepartmentsListComponent,
    StudyProgramsListComponent
  ],
  schemas: [
     CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class HaheUnitsModule { }
