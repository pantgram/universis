import { EventEmitter, Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ClientDataQueryable } from '@themost/client';
import { DIALOG_BUTTONS, ModalService, LoadingService, ErrorService } from '@universis/common';
import { ActivatedTableService, AdvancedRowActionComponent } from '@universis/ngx-tables';
import { DiagnosticsService } from '@universis/common';
import { Observable } from 'rxjs';
import { ActiveDepartmentService } from '../../registrar-shared/services/activeDepartmentService.service';
import { from } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { ConfigurationService } from '@universis/common';

export interface StudentDegreeTemplateConfiguration {
  degreeTemplates?: {
    enabled? :boolean;
  }
}

@Injectable()
export class StudentDegreeTemplateService {

  public refreshAction: EventEmitter<any> = new EventEmitter<any>();
  private selectedItems: { id: number }[] = [];
  public enabled = false;
  public enabled$ = from(this.diagnosticsService.hasService('eDiplomasService')).pipe(shareReplay(1));
  
  constructor(protected activatedTable: ActivatedTableService,
    protected activatedDepartment: ActiveDepartmentService,
    protected modalService: ModalService,
    protected context: AngularDataContext,
    protected translateService: TranslateService,
    protected loadingService: LoadingService,
    protected errorService: ErrorService,
    protected diagnosticsService: DiagnosticsService,
    protected configurationService: ConfigurationService) {
      //
      const config = this.configurationService.settings as StudentDegreeTemplateConfiguration;
      if (config.degreeTemplates && config.degreeTemplates.enabled) {
        this.enabled = true;
      }
  }

  async getSelectedItems(): Promise<{ id: number }[]> {
    let items = [];
    if (this.activatedTable.activeTable && this.activatedTable.activeTable.lastQuery) {
      const lastQuery: ClientDataQueryable = this.activatedTable.activeTable.lastQuery;
      if (lastQuery != null) {
        if (this.activatedTable.activeTable.smartSelect) {
          // get items
          const selectArguments = ['id'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.activatedTable.activeTable.unselected && this.activatedTable.activeTable.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.activatedTable.activeTable.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.activatedTable.activeTable.selected.map((item) => {
            return {
              id: item.id
            };
          });
        }
      }
    }
    return items;
  }

  setDegreeTemplateAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      const component = this.modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.degreeTemplate == null) {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      const selectedItems = this.selectedItems.map((item) => {
        return {
          id: item.id
        }
      });
      (async () => {
        let degreeTemplate = data.degreeTemplate;
        if (degreeTemplate === '') {
          degreeTemplate = null;
        }
        for (let index = 0; index < selectedItems.length; index++) {
          try {
            const item = selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            await this.context.model('Students').save({
              id: item.id,
              degreeTemplate: degreeTemplate
            });
            try {
              await this.activatedTable.activeTable.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.error(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  setDegreeTemplate() {
    Promise.all([
      this.activatedDepartment.getActiveDepartment(),
      this.getSelectedItems()
    ]).then((results) => {
      const department = results[0];
      this.selectedItems = results[1];
      const data = {
        department: department && department.alternativeCode
      };
      this.modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems, // set items
          data: data,
          modalTitle: this.translateService.instant('DegreeTemplates.SetDegreeTemplateAction'), // set title
          description: this.translateService.instant('DegreeTemplates.SetStudentDegreeTemplateDescription'),
          okButtonText: this.translateService.instant('Tables.Apply'),
          okButtonClass: 'btn btn-success',
          formTemplate: 'StudyProgramSpecialties/setDegreeTemplate',
          progressType: 'success',
          refresh: this.refreshAction, // refresh action event
          execute: this.setDegreeTemplateAction()
        }
      });
    }).catch((err) => {
      console.error(err);
      this.errorService.showError(err, {
        continueLink: '.'
      });
    });
  }

  clearDegreeTemplateAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      const selectedItems = this.selectedItems.map((item) => {
        return {
          id: item.id
        }
      });
      (async () => {
        for (let index = 0; index < selectedItems.length; index++) {
          try {
            const item = selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            await this.context.model('Students').save({
              id: item.id,
              degreeTemplate: null
            });
            try {
              await this.activatedTable.activeTable.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.error(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  clearDegreeTemplate() {
    Promise.all([
      this.getSelectedItems()
    ]).then((results) => {
      this.selectedItems = results[0];
      this.modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems, // set items
          data: {},
          modalTitle: this.translateService.instant('DegreeTemplates.ClearDegreeTemplateTitle'), // set title
          description: this.translateService.instant('DegreeTemplates.ClearDegreeTemplateMessage'),
          okButtonText: this.translateService.instant('Tables.Apply'),
          okButtonClass: 'btn btn-success',
          progressType: 'success',
          refresh: this.refreshAction, // refresh action event
          execute: this.clearDegreeTemplateAction()
        }
      });
    }).catch((err) => {
      console.error(err);
      this.errorService.showError(err, {
        continueLink: '.'
      });
    });
  }

}