import { EventsModule } from '@universis/ngx-events';
import { NgModule } from '@angular/core';
import { AppEventService, ErrorService, LoadingService } from '@universis/common';
import { CommonModule } from '@angular/common';
import { ReportsSharedModule } from '../reports-shared/reports-shared.module';
import { ReportService } from '../reports-shared/services/report.service';

@NgModule({
    imports: [
        CommonModule,
        ReportsSharedModule,
        EventsModule
    ]
})
export class EventsWrapperModule {
    constructor(
        private _appEvent: AppEventService,
        private _reportsService: ReportService,
        private _errorService: ErrorService,
        private _loadingService: LoadingService,
    ) { 
        this._appEvent.changed.subscribe(async ({model, action, reportTemplate, params} = {}) => {
            if (model === 'TimetableEvent' && action === 'print') {
                try {
                    this._loadingService.showLoading();
                    const blob = await this._reportsService.printReport(reportTemplate.id, params)
                    this._reportsService.downloadBlob(blob, reportTemplate.name)
                    this._loadingService.hideLoading();
                } catch (error) {
                    this._loadingService.hideLoading();
                    this._errorService.showError(error, {
                        continueLink: '.'
                    });
                }
            }
        })
    }
}
