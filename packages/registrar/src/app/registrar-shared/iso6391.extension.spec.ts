import './iso6391.extension';
import * as ISO6391 from 'iso-639-1/build';

describe('ISO9391 Extensions', () => {

  it('should use getNativeName', () => {
    expect(ISO6391.getNativeName('--')).toBeFalsy();
    expect(ISO6391.getNativeName('grc')).toEqual('Αρχαία Ελληνικά');
    expect(ISO6391.getNativeName('en')).toEqual('English');
  });

  it('should use getName', () => {
    expect(ISO6391.getName('grc')).toEqual('Ancient Greek');
    expect(ISO6391.getName('en')).toEqual('English');
  });

  it('should use getCode', () => {
    expect(ISO6391.getCode('Ancient Greek')).toEqual('grc');
    expect(ISO6391.getCode('English')).toEqual('en');
  });

  it('should use getAllNames', () => {
    const names = ISO6391.getAllNames();
    expect(names.length).toBeGreaterThan(0);
    expect(names.indexOf('Ancient Greek')).toBeGreaterThanOrEqual(0);
    expect(names.indexOf('English')).toBeGreaterThanOrEqual(0);
  });

  it('should use getAllCodes', () => {
    const codes = ISO6391.getAllCodes();
    expect(codes.length).toBeGreaterThan(0);
    expect(codes.indexOf('grc')).toBeGreaterThanOrEqual(0);
    expect(codes.indexOf('--')).toBe(-1);
    expect(codes.indexOf('el')).toBeGreaterThanOrEqual(0);
  });

  it('should use getAllNativeNames', () => {
    const names = ISO6391.getAllNativeNames();
    expect(names.length).toBeGreaterThan(0);
    expect(names.indexOf('Ελληνικά')).toBeGreaterThanOrEqual(0);
    expect(names.indexOf('Αρχαία Ελληνικά')).toBeGreaterThanOrEqual(0);
    expect(names.indexOf('English')).toBeGreaterThanOrEqual(0);
  });

  it('should use validate', () => {
    expect(ISO6391.validate('grc')).toBeTruthy();
    expect(ISO6391.validate('el')).toBeTruthy();
    expect(ISO6391.validate('--')).toBeFalsy();
  });
});
