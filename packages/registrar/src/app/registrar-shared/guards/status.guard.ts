import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
} from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { Observable } from 'rxjs';

export interface StatusGuardData {
  statusGuardParams: {
    /**
     * The entity in which the item belongs to (e.g. 'Students')
     */
    model: string;
    /**
     * The status field name (e.g. 'studentStatus')
     */
    statusProperty: string;
    /**
     * The alternateName value of the statusProperty (e.g. 'erased')
     */
    statusValue: string;
    extras?: {
      existsInRouteParamsAs?: string;
      redirectTo?: string;
      shouldActivateRoute?: boolean;
    };
  };
}

const STATUS_NESTED_PROPERTY = 'alternateName';

@Injectable()
export class StatusGuard implements CanActivate {
  constructor(private _context: AngularDataContext) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | boolean {
    return new Observable<boolean>((resolve) => {
      if (!this._isHandleableSnapshot(route)) {
        console.error(
          'At least one of the model/status/statusValue property is missing from the route data. Cannot allow route activation.'
        );
        return resolve.next(false);
      }
      const routeData = route.data as StatusGuardData;
      const model = routeData.statusGuardParams.model || route.data.model;
      // get primary key
      this._context.getMetadata().then((schema) => {
        const testName = new RegExp(`^${model}$`, 'i');
        // find entity set
        const findEntitySet = schema.EntityContainer.EntitySet.find((x) => {
          return testName.test(x.Name);
        });
        if (findEntitySet) {
          const findEntityType = schema.EntityType.find((x) => {
            return x.Name === findEntitySet.EntityType;
          });
          if (findEntityType) {
            // get primary key
            const key =
              (findEntityType.Key &&
                findEntityType.Key.PropertyRef[0] &&
                findEntityType.Key.PropertyRef[0].Name) ||
              'id';
            const query = this._context
              .model(findEntitySet.Name)
              .asQueryable()
              .prepare();
            return query
              .where(key)
              .equal(
                route.params[
                  routeData.statusGuardParams.extras &&
                    routeData.statusGuardParams.extras.existsInRouteParamsAs
                ] || route.params.id
              )
              .and(
                `${routeData.statusGuardParams.statusProperty}/${STATUS_NESTED_PROPERTY}`
              )
              .equal(routeData.statusGuardParams.statusValue)
              .select(key)
              .getItem()
              .then((allowRouteActivation) => {
                if (
                  routeData.statusGuardParams.extras &&
                  routeData.statusGuardParams.extras.shouldActivateRoute ===
                    false
                ) {
                  return resolve.next(allowRouteActivation == null);
                }
                return resolve.next(allowRouteActivation != null);
              })
              .catch((err) => {
                console.error(err);
                return resolve.next(false);
              });
          }
          console.error(
            'Cannot find the entity type of provided model. Cannot allow route activation.'
          );
          return resolve.next(false);
        }
        console.error(
          'Cannot find the entity set of provided model. Cannot allow route activation.'
        );
        return resolve.next(false);
      });
    });
  }

  private _isHandleableSnapshot(snapshot: ActivatedRouteSnapshot) {
    const statusGuardData: StatusGuardData =
      snapshot && (snapshot.data as StatusGuardData);
    return (
      statusGuardData &&
      statusGuardData.statusGuardParams &&
      statusGuardData.statusGuardParams.model &&
      statusGuardData.statusGuardParams.statusProperty &&
      statusGuardData.statusGuardParams.statusValue
    );
  }
}
