import {Injectable} from '@angular/core';
import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class TableConfigurationResolver implements Resolve<TableConfiguration> {

    constructor(private http: HttpClient) {
        //
    }

    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        const list = route.data.list || route.params.list;
        const model = route.data.model || route.params.model;
        return this.get(model, list);
    }

    get(model: string, list?: string): Observable<TableConfiguration> {
        if (list == null) {
            return this.http.get<TableConfiguration>(`assets/tables/${model}/config.list.json`);
        }
        return this.http.get(`assets/tables/${model}/config.${list}.json`).pipe(
            map((result) => {
                return result as TableConfiguration;
            }),
            catchError((err) => {
                if (err.status === 404) {
                    return this.http.get<TableConfiguration>(`assets/tables/${model}/config.list.json`);
                }
                throw err;
            })
        );
    }
}

@Injectable()
export class SearchConfigurationResolver implements Resolve<TableConfiguration> {

    constructor(private http: HttpClient) {
        //
    }

    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> {
        const list = route.data.list || route.params.list;
        const model = route.data.model || route.params.model;
        return this.get(model, list);
    }

    get(model: string, list?: string): Observable<any> {
        if (list == null) {
            return this.http.get<any>(`assets/tables/${model}/search.list.json`);
        }
        return this.http.get(`assets/tables/${model}/search.${list}.json`).pipe(
            map((result) => {
                return result as any;
            }),
            catchError((err) => {
                if (err.status === 404) {
                    return this.http.get<any>(`assets/tables/${model}/search.list.json`);
                }
                throw err;
            })
        );
    }
}
