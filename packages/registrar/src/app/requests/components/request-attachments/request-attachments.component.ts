import {Component, Input,  OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';



@Component({
  selector: 'app-request-attachments',
  templateUrl: './request-attachments.component.html'
})
export class RequestAttachmentsComponent implements OnInit {
  @Input('request') request;
  public attachments: any;

  constructor(private _context: AngularDataContext) {
  }

  async ngOnInit() {
    // get request logs
    const request = await this._context.model('StudentRequestActions')
      .where('id').equal(this.request.id)
      .expand('attachments($expand=attachmentType)')
      .orderBy('dateCreated')
      .getItem();
    this.attachments = request.attachments;
  }

  downloadFile(attachment) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachment.url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachment.name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }

}
