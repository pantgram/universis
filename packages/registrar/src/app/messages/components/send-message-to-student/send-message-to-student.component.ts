import {Component, OnInit, ViewChild, Input, Output, EventEmitter, ElementRef} from '@angular/core';
import { ErrorService, LoadingService, ModalService, DIALOG_BUTTONS, ToastService,  } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { MessagesService } from '../../services/messages.service';
import {Subscription} from 'rxjs';
import {OnDestroy} from '@angular/core/src/metadata/lifecycle_hooks';


@Component({
  selector: 'app-send-message-to-student',
  templateUrl: './send-message-to-student.component.html'
})
export class SendMessageToStudentComponent implements OnInit, OnDestroy {

  @Input() studentId: Array<number> = [];
  @Input() showLoading: boolean = true;
  @Input() modalTitle ?: string;
  @Input() description ?: string;
  @ViewChild('progress') progress: ElementRef;
  public refresh: any = new EventEmitter<any>();
  private refreshSubscription: Subscription;
  @Input() showMessageForm = false;   //  A value passed from parent component to define if the message send form should be displayed
  public messageModel = {             //  A structure that describes a message properties
    body: null,
    attachment: null,
    subject: null
  };

  @Output() successfulSend = new EventEmitter<boolean>();

  @ViewChild('fileInput') fileInput;

  constructor(private _errorService: ErrorService,
              private _loadingService: LoadingService,
              private _messagesService: MessagesService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _translate: TranslateService) { }

  ngOnInit() {
    if(!this.showLoading )
      this.refreshSubscription = this.refresh.subscribe((value) => {
        if (value && value.progress) {
          let progress = 0;
          if (value.progress < 0) {
            progress = 0;
          } else if (value.progress > 100) {
            progress = 100;
          } else {
            progress = value.progress;
          }
          $(this.progress.nativeElement).find('.progress-bar').css('width', `${progress}%`);
          this.showProgress();
        }
      });
  }

/*
 *  A function to initialize / reset our message data
 */
  initData() {
//  initialize values
    this.messageModel.attachment = null;
    this.messageModel.body = null;
    this.messageModel.subject = null;
  }

/*
 *  A function to sends a message.
 */
  save() {
    this._modalService.showDialog(this._translate.instant('Modals.ModalSendMessageTitle'),
      this._translate.instant('Modals.ModalSendMessageMessage'),
      DIALOG_BUTTONS.OkCancel)
      .then(async (res) => {
        if (res === 'ok') {
          let result = {
            errors: 0,
            successes: 0,
            total: this.studentId.length
          };
          !this.showLoading ?
            this.refresh.emit({
              progress: 1
            }) :
            this._loadingService.showLoading();
          await this.studentId.forEach((item, index) => {
            this._messagesService.sendMessageToStudent(item, this.messageModel)
              .subscribe(res => {
                // complete the action
                if (index === this.studentId.length - 1) {
                  this.initData();

                  if (this.showLoading) {
                    this._loadingService.hideLoading();
                  }
                  if (this._modalService.modalRef) {
                    this._modalService.modalRef.hide();
                  }
                }
                ++result.successes;
                this.messageSent(true, index);
              }, (err) => {
                if (this.showLoading) {
                  this._loadingService.hideLoading();
                }
                this._toastService.show(this._translate.instant('Modals.FailedSendMessageTitle'),
                  this._translate.instant('Modals.FailedSendMessageMessage'));
                let container = document.body.getElementsByClassName('universis-toast-container')[0];
                if (container != null) {
                  container.classList.add('toast-error');
                }
                ++result.errors;
                this.messageSent(false);
              });
          });
          if (result && result.errors && result.errors > 0) {
            // show message for partial success
            if (result.errors === 1) {
              this._toastService.show(this._translate.instant('Modals.FailedSendMessageTitle'), this._translate.instant(
                'Modals.FailedSendMessageMessage'));
            } else {
              this._toastService.show(this._translate.instant('Modals.FailedSendMessageTitle'), this._translate.instant(
                'Modals.FailedSendMessageMessages',
                result));
            }
          } else {
            result.total > 1 ?
              this._toastService.show(this._translate.instant('Modals.SuccessfulSendMessageTitle'),
                this._translate.instant('Modals.SuccessfulSendMessageMessages')) :
              this._toastService.show(this._translate.instant('Modals.SuccessfulSendMessageTitle'),
                this._translate.instant('Modals.SuccessfulSendMessageMessage', result));
          }
        }
      });
  }

/*
 *  A function to get the selected file
 */
  onFileChanged(event) {
    this.messageModel.attachment = this.fileInput.nativeElement.files[0];
  }

/*
 *  A function to reset the file of html input element
 */
  reset() {
    this.fileInput.nativeElement.value = '';
    this.messageModel.attachment = null;
  }

/*
 *  A function to emit status of message sent to parent component
 */
  messageSent(successful: boolean, index?: number) {
    this.successfulSend.emit(successful);
    if(!this.showLoading && index !=undefined && index > 0)
      this.refresh.emit({
        progress: Math.floor(((index + 1) / this.studentId.length) * 100)
      });
  }
  close() {
    this.showMessageForm = !this.showMessageForm;
  }

  showProgress() {
    $(this.progress.nativeElement).css('visibility', 'visible');
  }

  hideProgress() {
    $(this.progress.nativeElement).css('visibility', 'hidden');
  }

  cancel(){
    if(this._modalService.modalRef)
      this._modalService.modalRef.hide();
  }

  ngOnDestroy() {
    if(this.refreshSubscription && !this.refreshSubscription.closed)
      this.refreshSubscription.unsubscribe();
  }
}
