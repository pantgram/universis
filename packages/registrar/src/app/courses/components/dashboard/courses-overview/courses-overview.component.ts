import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-courses-overview',
  templateUrl: './courses-overview.component.html',
  styleUrls: ['./courses-overview.component.scss']
})
export class CoursesOverviewComponent implements OnInit, OnDestroy {
  public currentYear: any;
  private subscription: Subscription;

  constructor(public _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {

  this.subscription = this._activatedRoute.params.subscribe(async (params) => {
    this.currentYear = await this._context.model('Courses')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('department($expand=currentYear)')
      .select('department/currentYear/alternateName', 'name', 'courseStructureType')
      .getItem();
  });
}

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
