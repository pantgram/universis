import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import {
  DIALOG_BUTTONS,
  ErrorService,
  LoadingService,
  ModalService,
} from '@universis/common';
import {
  AdvancedSearchFormComponent,
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult,
  AdvancedTableSearchComponent,
} from '@universis/ngx-tables';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, tap } from 'rxjs/operators';

import * as AUTO_REGISTER_ACTION_RESULT_CONFIG from './action-results.config.list.json';
import * as AUTO_REGISTER_ACTION_RESULT_SEARCH_CONFIG from './action-results.search.list.json';

@Component({
  selector: 'app-auto-registration-results',
  templateUrl: './auto-registration-results.component.html',
})
export class AutoRegistrationResultsComponent implements OnInit, OnDestroy {
  public recordsTotal: number | string;
  public tableConfiguration: any;
  public searchConfiguration: any;
  public inProgressAction: any;
  public refreshSubscription: Subscription;
  public refreshResultsList$ = new Subject();
  private currentDepartment: number | string;
  private dataSubscription: Subscription;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly context: AngularDataContext,
    private readonly errorService: ErrorService,
    private readonly modalService: ModalService,
    private readonly translateService: TranslateService,
    private readonly loadingService: LoadingService
  ) {}

  ngOnInit() {
    this.dataSubscription = this.activatedRoute.data.subscribe(async (data) => {
      try {
        // get resolved active department
        this.currentDepartment = data.department && data.department.id;
        // set table query
        this.table.query = this.context
          .model('AutoRegisterClassActionResults')
          .where('action/department')
          .equal(this.currentDepartment)
          .orderByDescending('id')
          .prepare();
        // set search config
        this.searchConfiguration = AUTO_REGISTER_ACTION_RESULT_SEARCH_CONFIG;
        this.search.form = this.searchConfiguration;
        // assign active department
        Object.assign(this.search.form, {
          department: this.currentDepartment,
        });
        // and init
        this.search.ngOnInit();
        // set table config
        this.tableConfiguration = AUTO_REGISTER_ACTION_RESULT_CONFIG;
        this.table.config = AdvancedTableConfiguration.cast(
          this.tableConfiguration
        );
        this.table.ngOnInit();
        // get in progress action, if any
        this.inProgressAction = await this.context
          .model('AutoRegisterClassActions')
          .where('department')
          .equal(this.currentDepartment)
          .and('actionStatus/alternateName')
          .equal('ActiveActionStatus')
          .select('id', 'dateCreated')
          .getItem();

        this.refreshSubscription = this.refreshResultsList$
          .pipe(
            // show pseudo loading
            tap(() => this.loadingService.showLoading()),
            // allow activation once per second
            debounceTime(1000)
          )
          .subscribe(async () => {
            try {
              // refersh table
              if (this.table) {
                this.table.fetch();
              }
              // refresh in progress action
              this.inProgressAction = await this.context
                .model('AutoRegisterClassActions')
                .where('department')
                .equal(this.currentDepartment)
                .and('actionStatus/alternateName')
                .equal('ActiveActionStatus')
                .select('id', 'dateCreated')
                .getItem();
            } catch (err) {
              console.error(err);
            } finally {
              this.loadingService.hideLoading();
            }
          });
      } catch (err) {
        console.error(err);
        this.errorService.showError(err);
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.refreshSubscription) {
      this.refreshSubscription.unsubscribe();
    }
  }

  async cancelAutoRegisterClassesAction() {
    try {
      this.loadingService.showLoading();
      // revalidate action
      const autoRegisterAction = await this.context
        .model('AutoRegisterClassActions')
        .where('department')
        .equal(this.currentDepartment)
        .and('actionStatus/alternateName')
        .equal('ActiveActionStatus')
        .select('id')
        .getItem();
      const modalData: {
        ModalTitle: string;
        AlreadyCompletedMessage: string;
        CancelDescriptionMessage: string;
      } = this.translateService.instant(
        'Departments.AutoRegisterClassesAction.CancelAction.Modals'
      );
      // if action has been completed by the time
      // the user clicked the button
      if (autoRegisterAction == null) {
        // inform user
        this.modalService.showWarningDialog(
          modalData.ModalTitle,
          modalData.AlreadyCompletedMessage,
          DIALOG_BUTTONS.Ok
        );
        // nullify in progress action
        this.inProgressAction = null;
        // refresh results
        this.table.fetch(true);
        // and exit
        return Promise.resolve();
      }
      this.loadingService.hideLoading();
      // get confirmation
      const dialogResult = await this.modalService.showWarningDialog(
        modalData.ModalTitle,
        modalData.CancelDescriptionMessage
      );
      if (dialogResult !== 'ok') {
        return Promise.resolve();
      }
      // and cancel the action
      autoRegisterAction.actionStatus = {
        alternateName: 'CancelledActionStatus',
      };
      await this.context
        .model('AutoRegisterClassActions')
        .save(autoRegisterAction);
      // nullify in progress action on success
      this.inProgressAction = null;
      // and refresh table for the already produced results
      this.table.fetch(true);
    } catch (err) {
      console.error(err);
      this.errorService.showError(err, {
        continueLink: '.',
      });
    } finally {
      this.loadingService.hideLoading();
    }
  }
}
