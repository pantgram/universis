import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ModalService, ToastService, ErrorService, DIALOG_BUTTONS } from '@universis/common';
import { AdvancedTableConfiguration, AdvancedTableComponent, ActivatedTableService, AdvancedTableDataResult } from '@universis/ngx-tables';
import { Subscription } from 'rxjs';
import * as PLACES_LIST_CONFIG from './places.json';
import { AdminService, PermissionMask, Targets } from '@universis/ngx-admin';

@Component({
  selector: 'app-departments-preview-places',
  templateUrl: './departments-preview-places.component.html'
})
export class DepartmentsPreviewPlacesComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>PLACES_LIST_CONFIG;
  @ViewChild('places') places: AdvancedTableComponent;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  private subscription: Subscription;
  public departmentId: any;
  public model: any;
  public canEditPlaces = true;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _activatedTable: ActivatedTableService,
    private _modalService: ModalService,
    private _toastService: ToastService,
    private _translateService: TranslateService,
    private _errorService: ErrorService,
    private _adminService: AdminService,) { }

  ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      // get department id.
      this.departmentId = params.id;
      this._activatedTable.activeTable = this.places;
      // set places query
      this.places.query = this._context.model('Places/flattened')
        .asQueryable()
        .where('departments/id').equal(this.departmentId)
        .prepare();

      // check if the user has permission to edit department places
      try {
        this.canEditPlaces = await this._adminService.hasPermission({
          model: 'Place',
          privilege: 'Place/EditDepartments',
          target: Targets.All,
          mask: PermissionMask.Execute
        })
      } catch (error) {
        console.log(error)
      }

      // clone table config
      const config = AdvancedTableConfiguration.cast(PLACES_LIST_CONFIG);
      // if user is not allowed to edit department places, make table unselectable 
      if (!this.canEditPlaces) {
        config.selectable = false;
      }
      // set table config.
      this.places.config = config;
      // fetch places.
      this.places.fetch();
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.places.fetch();
        }
      });
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  removePlace() {
    if (this.places && this.places.selected && this.places.selected.length) {
      const items = this.places.selected.map(item => {
        const department = { id: this.departmentId };
        // set state for deletion.
        Object.defineProperty(department, '$state', {
          configurable: true,
          writable: true,
          enumerable: true,
          value: 4
        });
        return {
          id: item.id,
          departments: [department]
        };
      });
      // get confirmation.
      this._modalService.showWarningDialog(
        this._translateService.instant('Departments.RemovePlace'),
        this._translateService.instant('Departments.RemovePlaceMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            // save-remove items.
            this._context.model('Places').save(items).then(() => {
              // inform place.
              this._toastService.show(
                this._translateService.instant('Departments.RemovePlacesMessage.title'),
                this._translateService.instant((items.length === 1 ?
                  'Departments.RemovePlacesMessage.one' : 'Departments.RemovePlacesMessage.many')
                  , { value: items.length })
              );
              // fetch places.
              this.places.fetch();
            }).catch(err => {
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });
    }
  }
}
