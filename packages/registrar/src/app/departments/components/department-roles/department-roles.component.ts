import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult, initialState } from '@universis/ngx-tables';
import { Subscription } from 'rxjs';
import * as DEPARTMENT_ROLES_CONFIG from './department-roles-config.json';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { AppEventService, DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService } from '@universis/common';
import { AddDepartmentRoleComponent } from './add-department-role/add-department-role.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-department-roles',
  templateUrl: './department-roles.component.html',
  styleUrls: ['./department-roles.component.scss']
})
export class DepartmentRolesComponent implements AfterViewInit, OnDestroy {
  public recordsTotal: number;
  @ViewChild('roles') roles: AdvancedTableComponent;
  private paramSubscription: Subscription;
  private addedItemsSubscription: Subscription;
  private editedItemsSubscription: Subscription;
  private department: number | string;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly context: AngularDataContext,
    private readonly modalService: ModalService,
    private readonly errorService: ErrorService,
    private readonly appEvent: AppEventService,
    private readonly translateService: TranslateService,
    private readonly toastService: ToastService,
    private readonly loadingService: LoadingService
  ) {}

  ngAfterViewInit() {
    this.paramSubscription = this.activatedRoute.params.subscribe((params: { id: number | string }) => {
      try {
        // get current department
        this.department = params.id;
        // set table query
        this.roles.query = this.context.model('DepartmentRoles').where('department').equal(this.department).prepare();
        // set table config
        this.roles.config = AdvancedTableConfiguration.cast(DEPARTMENT_ROLES_CONFIG, true);
        // and fetch data
        this.roles.fetch();
      } catch (err) {
        console.error(err);
        this.errorService.navigateToError(err);
      }
    });
    this.addedItemsSubscription = this.appEvent.added.subscribe((event: { model: string }) => {
      try {
        // if a new role was added
        if (event && event.model === this.roles.config.model) {
          // refresh table
          this.roles.fetch();
        }
      } catch (err) {
        // just log the error
        console.error(err);
      }
    });
    this.editedItemsSubscription = this.appEvent.changed.subscribe((event: { model: string; target: { id: number | string } }) => {
      try {
        // if a specific role was edited
        if (event && event.model === this.roles.config.model && event.target && event.target.id) {
          // refresh the role
          this.roles.fetchOne({
            id: event.target.id
          });
        }
      } catch (err) {
        // just log the error
        console.error(err);
      }
    });
  }

  ngOnDestroy() {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.addedItemsSubscription) {
      this.addedItemsSubscription.unsubscribe();
    }
    if (this.editedItemsSubscription) {
      this.editedItemsSubscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  async addRole() {
    try {
      const department = await this.context.model('Departments').where('id').equal(this.department).select('id', 'name').getItem();
      if (department == null) {
        throw new Error('The current department cannot be found');
      }
      this.modalService.openModalComponent(AddDepartmentRoleComponent, {
        class: 'modal-lg',
        ignoreBackdropClick: true,
        closeOnSubmit: true,
        initialState: {
          data: {
            department
          }
        }
      });
    } catch (err) {
      console.error(err);
      this.errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async removeRole() {
    try {
      // get selected roles
      const selectedRoles = this.roles && this.roles.selected;
      if (!(Array.isArray(selectedRoles) && selectedRoles.length > 0)) {
        return;
      }
      // get confirmation from user
      const confirmationModal: {
        Title: string;
        Message: string;
      } = this.translateService.instant('Departments.Roles.Remove.ConfirmationModal');
      const actionConfirmationResult = await this.modalService.showWarningDialog(
        confirmationModal.Title,
        confirmationModal.Message,
        DIALOG_BUTTONS.OkCancel
      );
      if (actionConfirmationResult !== 'ok') {
        return;
      }
      // show loading
      this.loadingService.showLoading();
      // remove roles
      await this.context.model('DepartmentRoles').remove(
        selectedRoles.map((role) => {
          return {
            id: role.id,
            department: role.department
          };
        })
      );
      const successToast: {
        Title: string;
        Message: string;
      } = this.translateService.instant('Departments.Roles.Remove.SuccessToast');
      // show success toast
      this.toastService.show(successToast.Title, successToast.Message);
      // and refresh table
      return this.roles.fetch();
    } catch (err) {
      console.error(err);
      this.errorService.showError(err, {
        continueLink: '.'
      });
    } finally {
      // hide loading
      this.loadingService.hideLoading();
    }
  }
}
