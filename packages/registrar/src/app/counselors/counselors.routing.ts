import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CounselorsHomeComponent} from './components/counselors-home/counselors-home.component';
import {CounselorsTableComponent} from './components/counselors-table/counselors-table.component';
import {
  CounselorsTableConfigurationResolver,
  CounselorsTableSearchResolver
} from './components/counselors-table/counselors-table-config.resolver';
import {
  ActiveDepartmentIDResolver,
  CurrentAcademicPeriodResolver,
  CurrentAcademicYearResolver
} from '../registrar-shared/services/activeDepartmentService.service';
import {AdvancedFormItemResolver, AdvancedFormModalComponent, AdvancedFormModalData, AdvancedFormResolver} from '@universis/forms';
import {AdvancedFormRouterComponent} from '@universis/forms';

const routes: Routes = [
  {
    path: '',
    component: CounselorsHomeComponent,
    data: {
      title: 'Counselors'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/active'
      },
      {
        path: 'list',
        pathMatch: 'full',
        redirectTo: 'list/index'
      },
      {
        path: 'list/:list',
        component: CounselorsTableComponent,
        data: {
          title: 'Counselors List'
        },
        resolve: {
          currentYear: CurrentAcademicYearResolver,
          currentPeriod: CurrentAcademicPeriodResolver,
          tableConfiguration: CounselorsTableConfigurationResolver,
          searchConfiguration: CounselorsTableSearchResolver
        },
        children: [
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'StudentCounselors',
              action: 'edit',
              closeOnSubmit: true,
              serviceQueryParams: {
                $expand: 'instructor,toYear,toPeriod,student($expand=person)',
                $levels: 3
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          },
          {
            path: ':action',
            component: AdvancedFormRouterComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class CounselorsRoutingModule {
}
