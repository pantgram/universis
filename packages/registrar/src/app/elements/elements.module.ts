import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {CardCollapsibleComponent} from './components/card-collapsible/card-collapsible.component';
import {FormCollapsibleComponent} from './components/form-collapsible/form-collapsible.component';
import {FormDatepickerComponent} from './components/form-datepicker/form-datepicker.component';
import {FormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule
    ],
    declarations: [
        FormCollapsibleComponent,
        CardCollapsibleComponent,
        FormDatepickerComponent
    ],
    providers: [
    ],
    exports: [
        FormCollapsibleComponent,
        CardCollapsibleComponent,
        FormDatepickerComponent
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ]
})
export class ElementsModule {

}
