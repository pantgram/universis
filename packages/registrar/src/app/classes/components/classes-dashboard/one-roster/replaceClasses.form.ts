const replaceClassesForm = {
    "model": "CourseClasses",
    "settings": {
      "i18n": {
        "el": {
          "Save": "Αποθήκευση"
        },
        "en": {
        }
      }
    },
    "components": [
      {
        "type": "fieldset",
        "components": [
          {
            "columns": [
              {
                "components": [
                  {
                    "customClass": "formio-component-select-wrap",
                    "hideLabel": true,
                    "labelPosition": "top",
                    "customConditional": "show = data.courseClass != null",
                    "widget": "choicesjs",
                    "dataSrc": "url",
                    "data": {
                      "url": "/CourseClasses?$top={{limit}}&$skip={{skip}}&$select=id,course/displayCode as displayCode,course/department/name as departmentName,title,year/name as yearName,period/name as periodName&$filter=(indexof(title, '{{encodeURIComponent(search||'')}}') ge 0) and year eq {{data.courseClass.year}} and id ne '{{data.courseClass.id}}'&$orderby=title",
                      "headers": []
                    },
                    "validate": {
                      "required": false
                    },
                    "limit": 50,
                    "searchField": "search",
                    "searchEnabled": true,
                    "idProperty": "id",
                    "lazyLoad": false,
                    "multiple": true,
                    "dataType": "object",
                    "template": "({{ item.displayCode }}) {{ item.title }} {{item.yearName}}/{{item.periodName}} ({{ item.departmentName }})",
                    "selectThreshold": 0.3,
                    "key": "courseClasses",
                    "type": "select",
                    "selectValues": "value",
                    "input": true
                  }
                ],
                "width": 8
              }
            ],
            "customClass": "text-dark",
            "hideLabel": true,
            "type": "columns",
            "input": false
          },
          {
            "label": "Save",
            "action": "event",
            "showValidations": false,
            "theme": "theme",
            "tableView": false,
            "key": "save",
            "type": "button",
            "event": "replace",
            "input": true
          }
        ]
      }
    ],
    "width": 3
  }
  
  export {
    replaceClassesForm
  }