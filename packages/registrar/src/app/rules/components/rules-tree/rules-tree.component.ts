import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ErrorService } from '@universis/common';
import { Tree, TreeSettings } from 'gijgo';
import * as esprima from 'esprima';

declare var $: any;

@Component({
  selector: 'app-rules-tree',
  templateUrl: './rules-tree.component.html'
})
export class RulesTreeComponent implements AfterViewInit, OnDestroy, OnInit {
  @ViewChild('tree') treeRef: ElementRef;
  @Input() rules: any;
  @Output() ruleExpressionChange = new EventEmitter<any>();
  @Output() back = new EventEmitter<any>();

  public tree: Tree;
  public operators = ['&&', '||', '!'];
  public lastId = 0;
  public expression = '';
  public expressionToPost = '';
  public isValidExp = true;
  public oneRuleExpression = false;
  public isLoading = true;
  public warningMessage: string = null;

  private expressionFormatter = {
    // Format the expression so that esprima is able to parse it.
    // Keep only rule ids and replace operator literals with the corresponding symbols
    toEsprima: (exp: string) => exp.replace(/\[\%(\d+)\]/g, '$1').replace(/AND|OR|NOT/gi, op => this.opsMapping.symbol(op)),

    // Enclose rule ids in [% ] and replace operator symbols with the corresponding literals
    toPost: (exp: string) => exp.replace(/\&\&|\|\||\!/g, op => this.opsMapping[op].literal).replace(/(\d+)/g, '[%$1]'),

    // translate operator symbols
    toDisplay: (exp: string) => exp.replace(/\&\&|\|\||\!/g, op => this.opsMapping[op].translation)
  };

  private opsMapping = {
    symbol(literal: string) {
      return Object.keys(this).find(key => this[key]['literal'] === literal.toUpperCase());
    },
    '&&': {
      // tslint:disable-next-line: max-line-length
      template: `<div class="operator-container"><div class="operator"><span>${this._translateService.instant('Rules.LogicalOperators.&&')}</span></div></div>`,
      translation: this._translateService.instant('Rules.LogicalOperators.&&'),
      literal: 'AND'
    },
    '||': {
      // tslint:disable-next-line: max-line-length
      template: `<div class="operator-container"><div class="operator"><span>${this._translateService.instant('Rules.LogicalOperators.||')}</span></div></div>`,
      translation: this._translateService.instant('Rules.LogicalOperators.||'),
      literal: 'OR'
    },
    '!': {
      // tslint:disable-next-line: max-line-length
      template: `<div class="operator-container"><div class="operator"><span>${this._translateService.instant('Rules.LogicalOperators.!')}</span></div></div>`,
      translation: this._translateService.instant('Rules.LogicalOperators.!'),
      literal: 'NOT'
    }
  };
  private dataSourceNode = {
    text: null,
    value: null,
    type: null,
    id: null,
    children: []
  };
  private configuration: TreeSettings = {
    primaryKey: 'id',
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fontawesome',
    icons: {
      expand: '<i class="fa fa-chevron-up mr-2"></i>',
      collapse: '<i class="fa fa-chevron-down mr-2"></i>'
    },
    dragAndDrop: true
  };

  constructor(
    private _errorService: ErrorService,
    private _translateService: TranslateService) {}


  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    try {
      if (this.rules.length === 1) { this.oneRuleExpression = true; }
      const initialExpression = this.rules[0].ruleExpression || this.getDefaultExpression();
      this.configTreeDataSource(initialExpression);
      this.isLoading = false;
    } catch (error) {
      console.error(error);
      return this._errorService.navigateToError(error);
    }
  }

  /**
 * Calculates a default AND expression with the available rules.
 */
  getDefaultExpression(): string | '' {
    let ruleExpression = '';
    if (this.rules.length > 1) {
      ruleExpression += this.rules[0].id;
      for (const rule of this.rules.slice(1)) { ruleExpression += ` AND ${rule.id}`; }
    }
    return ruleExpression;
  }

  /**
  * Calculates the tree data based on a rule expression.
  * @param {string} exp - A rule expression.
  * @returns A nested object that represents the tree's data.
  */
  getInitialTreeData(exp: string) {
    exp = this.expressionFormatter.toEsprima(exp);
    const parsedExp = esprima.parseScript(exp);
    const treeData = JSON.parse(JSON.stringify(this.dataSourceNode));
    this.parse(parsedExp.body[0]['expression'], treeData);
    return treeData;
  }

  configTreeDataSource(exp: string) {
    if (exp) {
      const treeData = this.getInitialTreeData(exp);
      this.configuration.dataSource = [treeData];
      this.getExpression(treeData);
    } else {
      this.configuration.dataSource = [];
    }
  }

  /**
  * Adds a unary NOT expression with the available rule to the tree.
  */
  setNotExpression() {
    const initialExpression = 'NOT ' + this.rules[0].id;
    const node = this.getInitialTreeData(initialExpression);
    this.tree.addNode(node, null, null);
    this.tree.expandAll();
  }

  /**
 * Destroys the tree and rebuilds it with the default AND expression
 */
  resetExpression() {
    this.ngOnDestroy();
    this.expression = '';
    this.expressionToPost = '';
    this.lastId = 0;
    const exp = this.getDefaultExpression();
    this.configTreeDataSource(exp);
    this.ngAfterViewInit();
  }

  /**
  * Parses an esprima expression recursively and configures a data source compatible with the gijgo tree.
  * @param exp - A parsed esprima expression.
  * @param node - The tree node to be configured.
  */
  parse(exp, node) {
    // if the id of the node is not null, then the node has already been configured
    if (!node.id) {
      if (exp.type === 'Literal') {
        const currentRule = this.rules.find(rule => rule.id === Number(exp.value));
        const currentRuleDescription = currentRule.description || this._translateService.instant(`Rules.${currentRule.refersTo}Rule.Title`);
        node.text = `${currentRule.id} - ${currentRuleDescription}`;
        node.value = currentRule;
        node.type = 'rule';
        node.id = ++this.lastId;
      }
      if (exp.operator) {
        node.text = this.opsMapping[exp.operator].template;
        node.value = exp.operator;
        node.type = 'operator';
        node.id = ++this.lastId;
      }
      if (exp.type === 'UnaryExpression') {
        // push a deep copy of an empty dataSourceNode
        const index = node.children.push(JSON.parse(JSON.stringify(this.dataSourceNode)));
        this.parse(exp.argument, node.children[index - 1]);
      }
    }

    if (exp.left && !(exp.left.operator && exp.operator === exp.left.operator)) {
      // push a deep copy of an empty dataSourceNode
      const index = node.children.push(JSON.parse(JSON.stringify(this.dataSourceNode)));
      this.parse(exp.left, node.children[index - 1]);
    } else if (exp.left) {
      this.parse(exp.left, node);
    }

    if (exp.right && !(exp.right.operator && exp.operator === exp.right.operator)) {
      // push a deep copy of an empty dataSourceNode
      const index = node.children.push(JSON.parse(JSON.stringify(this.dataSourceNode)));
      this.parse(exp.right, node.children[index - 1]);
    } else if (exp.right) {
      this.parse(exp.right, node);
    }
  }

  /**
  * Calculates the rule expressions that will be posted and displayed.
  * @param data - The tree's base node.
  */
  getExpression(data = null) {
    try {
      this.isValidExp = true;
      this.warningMessage = null;

      const treeData = data || this.tree.getAll()[0];
      let flatExp = this.flatten(treeData, '');
      if (flatExp && this.isValidExp) {
        // All expressions, except those that start with a NOT operator, are enclosed in '()'.
        // Remove redundant parentheses and then add whitespace between operators and rules.
        let shouldPost = false;
        flatExp = flatExp[0] === '(' ? flatExp.substring(1, flatExp.length - 1) : flatExp;
        flatExp = flatExp.replace(/\&\&|\|\||\!/g, op => {
          if (op !== '&&') { shouldPost = true; } // expressions that include only AND operators should not be posted
          return op === '!' ?  `${op} ` : ` ${op} `;
        });

        // A rule expression must include all the input rules in order to be valid.
        const rulesUsed = Array.from(new Set(flatExp.match(/\d+/g)));
        for (const rule of this.rules) {
          if (!rulesUsed.includes(String(rule.id))) {
            this.isValidExp = false;
            this.warningMessage = 'UnusedRules';
            break;
          }
        }

        this.expression = this.expressionFormatter.toDisplay(flatExp);
        this.expressionToPost = shouldPost ? this.expressionFormatter.toPost(flatExp) : '';
      }
      this.ruleExpressionChange.emit(this.isValidExp);
    } catch (error) {
      console.error(error);
      return this._errorService.navigateToError(error);
    }
  }

  /**
  * Recursively flattens a rule tree to a string rule expression.
  * @param node - The current tree node.
  * @param flat - The current rule expression.
  * @returns {string} The flattened expression or an empty string if it's invalid.
  */
  flatten(node, flat: string): string | '' {
    if (!this.isValidExp) { return ''; }

    if (node.type === 'operator') {
      if (!this.validOpNode(node)) { return; }
      if (node.value === '!') { flat += node.value; } else { flat += '('; }
      if (node.children) {
        node.children.forEach((child, index) => {
          flat = this.flatten(child, flat);
          if (node.value !== '!' && index < (node.children.length - 1)) { flat += node.value; }
        });
      }
      if (node.value !== '!') { flat += ')'; }
    } else if (node.type === 'rule') {
      flat += node.value.id;
    }

    return flat;
  }

  validOpNode(op) {
    if (op.value === '!') {
      if (op.children && op.children.length === 1) { return true; }
      this.warningMessage = 'InvalidNotExp';
    }
    if (op.value !== '!') {
      if (op.children && op.children.length >= 2) { return true; }
      this.warningMessage = op.value === '&&' ? 'InvalidAndExp' : 'InvalidOrExp';
    }

    this.isValidExp = false;
    return false;
  }


  ngAfterViewInit() {
    if (this.treeRef) {
      try {
        this.tree = $(this.treeRef.nativeElement).tree(this.configuration);
        this.tree.expandAll();

        const tree = this.tree;
        const self = this;
        tree.on('nodeDrop', function (e, draggedId, droppedOnId, orderNumber) {
          if (draggedId === '1') { return false; }

          // can't use self.getExpression() to validate the resulting expression cause
          // the tree's data are updated after the event is fired. As a workaround,
          // we change te position of the node manually, so that the 'nodeDataBound' event is fired.
          const draggedDataNode = tree.getDataById(String(draggedId));
          const draggedNode = tree.getNodeById(String(draggedId));

          // if the dragged node is dropped on a rule node, insert it to the same parent node
          const droppedDataNode = tree.getDataById(droppedOnId);
          const targetParentNode = droppedDataNode.type === 'rule'
            ? tree.getNodeById(self.getParentNodeId(droppedOnId))
            : tree.getNodeById(String(droppedOnId));
          tree.removeNode(draggedNode);
          tree.addNode(draggedDataNode, targetParentNode, 1);

          // rebuild the tree so that it's expanded correctly
          self.configuration.dataSource = tree.getAll();
          const treeData = self.configuration.dataSource[0];
          self.changeOrder(treeData, draggedId);
          self.getExpression(treeData);
          self.ngOnDestroy();
          self.ngAfterViewInit();
          return false;
        });

        this.tree.on('select', function (e, node, id) {
          const nodeData = tree.getDataById(id);
          if (nodeData.type === 'operator') {
            $('#operator').prop('disabled', false);
            $('#operator').val(nodeData.value);
          } else {
            $('#operator').prop('disabled', true);
          }
        });

        this.tree.on('nodeDataBound', function () {
          self.getExpression();
        });

      } catch (error) {
        console.error(error);
        return this._errorService.navigateToError(error);
      }
    }
  }

  changeOrder(treeData, currentId) {
    const parent = this.findParentNode(treeData, currentId);
    const last = parent.children.pop();
    parent.children.unshift(last);
  }

  getParentNodeId(currentId) {
    const treeData = this.tree.getAll()[0];
    return Number(currentId) === 1 ? '1' : String(this.findParentNode(treeData, Number(currentId)).id);
  }

  findParentNode (node, id) {
    let found = null;
    if (node.children && node.children.length) {
      for (const child of node.children) {
        if (child.id === id) {
          found = node;
        } else {
          found = this.findParentNode(child, id);
        }
        if (found) { break; }
      }
    }
    return found;
  }

  save() {
    // The select element is disabled when a rule is selected. Rule nodes shouldn't change, so return.
    if ($('#operator').prop('disabled')) { return; }

    // Get the selected node's id. If no node is selected return
    const id = this.tree.getSelections()[0];
    if (!id) { return; }

    const record = this.tree.getDataById(id);
    record.value = $('#operator').val();
    record.text = this.opsMapping[record.value].template;
    this.tree.updateNode(id, record);
  }

  remove() {
    // Get the selected node's id. If no node is selected return
    const id = this.tree.getSelections()[0];
    if (!id || id === '1') { return; }

    // if the node is an only child, remove parent node as well
    const parentNodeData = this.tree.getDataById(this.getParentNodeId(id));
    const nodeToRemove = (parentNodeData && parentNodeData.id !== 1 && parentNodeData.children.length === 1)
      ? this.tree.getNodeById(String(parentNodeData.id))
      : this.tree.getNodeById(String(id));

    this.tree.removeNode(nodeToRemove);
    this.getExpression();
  }

  /**
  * Appends an operator/rule to the selected node or to the root node depending on the selected node's type.
  * @param rule - A rule object.
  */
  append(rule = null) {
    const parent = this.getOpNode();
    const op = String($('#operator').val());
    const newNode = {
        text: rule
              ? `${rule.id} - ${rule.description || this._translateService.instant(`Rules.${rule.refersTo}Rule.Title`)}`
              : this.opsMapping[op].template,
        id: ++this.lastId,
        type: rule ? 'rule' : 'operator',
        value: rule || op
      };
    this.tree.addNode(newNode, parent, null);
  }

  /**
   * Returns the selected node in case it's an operator node, otherwise it returns the root node.
   */
  getOpNode() {
    const id = this.tree.getSelections()[0];
    return (id && this.tree.getDataById(id).type === 'operator')
      ? this.tree.getNodeById(String(id))
      : this.tree.getNodeById(this.getParentNodeId(id || '1'));
  }

  saveAndHide() {
    this.back.emit();
  }

  ngOnDestroy() {
    if (this.tree) { this.tree.destroy(); }
  }

}
