import { RulesModule } from './rules.module';
import {async, inject, TestBed} from '@angular/core/testing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {AngularDataContext, MostModule} from '@themost/angular';
import {ApiTestingModule, TestingConfigurationService} from '@universis/common/testing';
import {APP_BASE_HREF} from '@angular/common';
import {ConfigurationService} from '@universis/common';
import {RuleService} from './services/rule.service';

describe('RulesModule', () => {

  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        }),
        ApiTestingModule.forRoot()
      ],
      providers: [
        {
          provide: APP_BASE_HREF,
          useValue: '/'
        },
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        }
      ],
    }).compileComponents();
  }));

  it('should create an instance', inject([TranslateService],
    (translateService: TranslateService) => {
      const service = new RulesModule(null, translateService);
      expect(service).toBeTruthy();
    }));
});
