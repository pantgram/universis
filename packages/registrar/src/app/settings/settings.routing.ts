import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {ListComponent, EnumerationModelFormResolver, EnumerationModalTitleResolver} from './components/list/list.component';
import {SectionsComponent} from './components/sections/sections.component';
import {AdvancedFormModalComponent, AdvancedFormModalData, AdvancedFormItemResolver, AdvancedFormResolver} from '@universis/forms';
import {AdvancedFormModelResolver} from '@universis/forms';
import {AdvancedListComponent, AdvancedSearchConfigurationResolver} from '@universis/ngx-tables';
import {AdvancedTableConfigurationResolver} from '@universis/ngx-tables';
import {AdvancedFormItemWithLocalesResolver} from '@universis/forms';
import {ActiveDepartmentIDResolver, ItemLocalesArrayResolver} from '../registrar-shared/services/activeDepartmentService.service';
import {
  CandidateSourcesAttachSchemaComponent
} from './components/candidate-sources/candidate-sources-attach-schema/candidate-sources-attach-schema.component';
import { AdvancedListWrapperComponent } from './components/candidate-sources/advanced-list-wrapper/advanced-list-wrapper.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {
      title: 'Settings'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'sections'
      },
      {
        path: 'sections',
        component: SectionsComponent
      },
      {
        path: 'lists/AttachmentTypes',
        component: AdvancedListComponent,
        data: {
          model: 'AttachmentTypes',
          list: 'active',
          description: 'Settings.Lists.AttachmentType.Description',
          longDescription: 'Settings.Lists.AttachmentType.LongDescription',
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              description: null,
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'edit',
              serviceQueryParams: {
                $expand: 'locales',
              },
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemWithLocalesResolver
            }
          }
        ]
      },
      {
        path: 'lists/CourseTypes',
        component: AdvancedListComponent,
        data: {
          model: 'CourseTypes',
          list: 'active',
          description: 'Settings.Lists.CourseTypes.Description',
          longDescription: 'Settings.Lists.CourseTypes.LongDescription',
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              description: null,
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'edit',
              serviceQueryParams: {
                $expand: 'locales'
              },
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemWithLocalesResolver
            }
          }
        ]
      },
      {
        path: 'lists/CourseAreas',
        component: AdvancedListComponent,
        data: {
          model: 'CourseAreas',
          list: 'department',
          description: 'Settings.Lists.CourseAreas.Description',
          longDescription: 'Settings.Lists.CourseAreas.LongDescription'
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'CourseAreas',
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              department: ActiveDepartmentIDResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'CourseAreas',
              action: 'edit',
              closeOnSubmit: true,
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/Places',
        component: AdvancedListComponent,
        data: {
          model: 'Places',
          list: 'places',
          description: 'Settings.Lists.Places.Description',
          longDescription: 'Settings.Lists.Places.LongDescription'
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver,
          searchConfiguration: AdvancedSearchConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'Places',
              action: 'newEdit',
              closeOnSubmit: true,
              description: null,
              modalOptions: {
                modalTitle: 'Settings.NewItem'
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'Places',
              action: 'newEdit',
              closeOnSubmit: true,
              description: null,
              modalOptions: {
                modalTitle: 'Settings.EditItem'
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/ExceptHoursSpecifications',
        component: AdvancedListComponent,
        data: {
          model: 'ExceptHoursSpecifications',
          list: 'index',
          description: 'Settings.Lists.ExceptHoursSpecifications.Description',
          longDescription: 'Settings.Lists.ExceptHoursSpecifications.LongDescription'
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'ExceptHoursSpecifications',
              action: 'new',
              closeOnSubmit: true,
              description: null,
              modalOptions: {
                modalTitle: 'Settings.NewItem'
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'ExceptHoursSpecifications',
              action: 'edit',
              closeOnSubmit: true,
              description: null,
              modalOptions: {
                modalTitle: 'Settings.EditItem'
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/CourseSectors',
        component: AdvancedListComponent,
        data: {
          model: 'CourseSectors',
          list: 'department',
          description: 'Settings.Lists.CourseSectors.Description',
          longDescription: 'Settings.Lists.CourseSectors.LongDescription'
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'CourseSectors',
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              department: ActiveDepartmentIDResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'CourseSectors',
              action: 'edit',
              closeOnSubmit: true,
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/ExamPeriods',
        component: AdvancedListComponent,
        data: {
          model: 'ExamPeriods',
          list: 'active',
          description: 'Settings.Lists.ExamPeriods.Description',
          longDescription: 'Settings.Lists.ExamPeriods.LongDescription',
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'ExamPeriods',
              action: 'new',
              closeOnSubmit: true,
              data: {
                '$state': 1
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              department: ActiveDepartmentIDResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'ExamPeriods',
              action: 'edit',
              closeOnSubmit: true,
              serviceQueryParams: {
                $expand: 'locales, academicPeriods($select=id,name)'
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemWithLocalesResolver
            }
          }
        ]
      },
      {
        path: 'lists/ThesisSubjects',
        component: AdvancedListComponent,
        data: {
          model: 'ThesisSubjects',
          list: 'department',
          description: 'Settings.Lists.ThesisSubjects.Description',
          longDescription: 'Settings.Lists.ThesisSubjects.LongDescription',
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'ThesisSubjects',
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              department: ActiveDepartmentIDResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'ThesisSubjects',
              action: 'edit',
              closeOnSubmit: true,
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/AcademicYears',
        component: AdvancedListComponent,
        data: {
          model: 'AcademicYears',
          list: 'active',
          description: 'Settings.Lists.AcademicYear.LongDescription',
          longDescription: 'Settings.Lists.AcademicYear.LongDescription',
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'AcademicYears',
              action: 'new',
              closeOnSubmit: true,
              description: null
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              department: ActiveDepartmentIDResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'AcademicYears',
              action: 'edit',
              closeOnSubmit: true,
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/StudyLevels',
        component: AdvancedListComponent,
        data: {
          model: 'StudyLevels',
          list: 'index',
          description: 'Settings.Lists.StudyLevel.Description',
          category: 'Settings.Title'
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'new',
              closeOnSubmit: true,
              description: null
            },
            resolve: {
              modalOptions: EnumerationModalTitleResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'edit',
              closeOnSubmit: true,
              serviceQueryParams: {
                $expand: 'locales'
              }
            },
            resolve: {
              data: AdvancedFormItemWithLocalesResolver,
              modalOptions: EnumerationModalTitleResolver
            }
          }
        ]
      },
      {
        path: 'lists/ActionStatusArticles',
        component: AdvancedListComponent,
        data: {
          model: 'ActionStatusArticles',
          list: 'active',
          description: 'Settings.Lists.ActionStatusArticles.Description',
          longDescription: 'Settings.Lists.ActionStatusArticles.LongDescription'
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'ActionStatusArticles',
              action: 'new',
              closeOnSubmit: true,
              description: null
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'ActionStatusArticles',
              action: 'edit',
              closeOnSubmit: true,
              serviceQueryParams: {
                $expand: 'actionStatus'
              },
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/CandidateSources',
        component: AdvancedListWrapperComponent,
        data: {
          model: 'CandidateSources',
          list: 'active',
          description: 'Settings.Lists.CandidateSource.Description',
          longDescription: 'Settings.Lists.CandidateSource.LongDescription',
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'CandidateSources',
              action: 'new',
              closeOnSubmit: true,
              description: null
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'CandidateSources',
              action: 'edit',
              closeOnSubmit: true,
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          },
          {
            path: ':id/attachSchema',
            pathMatch: 'full',
            component: CandidateSourcesAttachSchemaComponent,
            outlet: 'modal'
          }
        ]
      },

      {
        path: 'lists/Nationalities',
        component: AdvancedListComponent,
        data: {
          model: 'Nationalities',
          list: 'index',
          description: 'Settings.Lists.Nationalities.Description',
          longDescription: 'Settings.Lists.Nationalities.LongDescription'
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'Nationalities',
              action: 'new',
              closeOnSubmit: true,
              description: null
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'Nationalities',
              action: 'edit',
              serviceQueryParams: {
                $expand: 'locales',
              },
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemWithLocalesResolver
            }
          }
        ]
      },
      {
        path: 'lists/MediaTypes',
        component: AdvancedListComponent,
        data: {
          model: 'MediaTypes',
          list: 'active',
          description: 'Settings.Lists.MediaType.Description',
          longDescription: 'Settings.Lists.MediaType.LongDescription',
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'MediaTypes',
              action: 'newEdit',
              closeOnSubmit: true,
              description: null,
              modalOptions: {
                modalTitle: 'Settings.NewItem'
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'MediaTypes',
              action: 'newEdit',
              closeOnSubmit: true,
              modalOptions: {
                modalTitle: 'Settings.EditItem'
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/AcademicCardConfigurations',
        component: AdvancedListComponent,
        data: {
          model: 'AcademicCardConfigurations',
          list: 'active',
          description: 'Settings.Lists.AcademicCardConfigurations.Description',
          longDescription: 'Settings.Lists.AcademicCardConfigurations.LongDescription',
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'AcademicCardConfigurations',
              action: 'new',
              closeOnSubmit: true,
              description: null,
              modalOptions: {
                modalTitle: 'Settings.NewItem'
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'AcademicCardConfigurations',
              action: 'edit',
              closeOnSubmit: true,
              modalOptions: {
                modalTitle: 'Settings.EditItem'
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/DepartmentStudentSeries',
        component: AdvancedListComponent,
        data: {
          model: 'DepartmentStudentSeries',
          list: 'department',
          description: 'Settings.Lists.DepartmentStudentSeries.Description',
          longDescription: 'Settings.Lists.DepartmentStudentSeries.LongDescription'
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'DepartmentStudentSeries',
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              department: ActiveDepartmentIDResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'DepartmentStudentSeries',
              action: 'edit',
              closeOnSubmit: true,
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      },
      {
        path: 'lists/Countries',
        component: AdvancedListComponent,
        data: {
          model: 'Countries',
          list: 'index',
          description: 'Settings.Lists.Country.Description',
          longDescription: 'Settings.Lists.Country.LongDescription',
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              description: null,
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'edit',
              serviceQueryParams: {
                $expand: 'locales',
              },
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemWithLocalesResolver
            }
          }
        ]
      },
      {
        path: 'lists/Genders',
        component: AdvancedListComponent,
        data: {
          model: 'Genders',
          list: 'index',
          description: 'Settings.Lists.Gender.Description',
          longDescription: 'Settings.Lists.Gender.LongDescription',
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              description: null,
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'edit',
              serviceQueryParams: {
                $expand: 'locales',
              },
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemWithLocalesResolver
            }
          }
        ]
      },
      {
        path: 'lists/IdentityTypes',
        component: AdvancedListComponent,
        data: {
          model: 'IdentityTypes',
          list: 'index',
          description: 'Settings.Lists.IdentityType.Description',
          longDescription: 'Settings.Lists.IdentityType.LongDescription',
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              description: null,
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'edit',
              serviceQueryParams: {
                $expand: 'locales',
              },
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemWithLocalesResolver
            }
          }
        ]
      },
      {
        path: 'lists/FamilyStatuses',
        component: AdvancedListComponent,
        data: {
          model: 'FamilyStatuses',
          list: 'index',
          description: 'Settings.Lists.FamilyStatus.Description',
          longDescription: 'Settings.Lists.FamilyStatus.LongDescription',
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              description: null,
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'edit',
              serviceQueryParams: {
                $expand: 'locales',
              },
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemWithLocalesResolver
            }
          }
        ]
      },
      {
        path: 'lists/RoleTypes',
        component: AdvancedListComponent,
        data: {
          model: 'RoleTypes',
          list: 'index',
          description: 'Settings.Lists.RoleType.Description',
          longDescription: 'Settings.Lists.RoleType.LongDescription',
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              description: null,
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'edit',
              serviceQueryParams: {
                $expand: 'locales, titles',
              },
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemWithLocalesResolver
            }
          }
        ]
      },
      {
        path: 'lists/SuspensionReasons',
        component: AdvancedListComponent,
        data: {
          model: 'SuspensionReasons',
          list: 'index',
          description: 'Settings.Lists.SuspensionReason.Description',
          longDescription: 'Settings.Lists.SuspensionReason.LongDescription',
        },
        resolve: {
          tableConfiguration: AdvancedTableConfigurationResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              description: null,
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'edit',
              serviceQueryParams: {
                $expand: 'locales',
              },
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemWithLocalesResolver
            }
          }
        ]
      },
      {
        path: 'lists/:model',
        component: ListComponent,
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              model: AdvancedFormModelResolver,
              formConfig: EnumerationModelFormResolver,
              modalOptions: EnumerationModalTitleResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'edit',
              closeOnSubmit: true
            },
            resolve: {
              model: AdvancedFormModelResolver,
              data: AdvancedFormItemResolver,
              formConfig: EnumerationModelFormResolver,
              modalOptions: EnumerationModalTitleResolver
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class SettingsRoutingModule {
}
