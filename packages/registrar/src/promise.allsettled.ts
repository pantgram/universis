export declare interface PromiseAllShettled<T> extends Promise<T> {
    allShettled(): any;
}

if (typeof (Promise as any).allShettled === 'undefined') {
    (Promise as any).allSettled = (promises: any) =>
    Promise.all(
      promises.filter( (promise: any) => {
          return promise != null;
      }).map((promise: any, i: any) =>
        promise.then(value => ({
            status: 'fulfilled',
            value,
          }))
          .catch(reason => ({
            status: 'rejected',
            reason,
          }))
      )
    );
}
