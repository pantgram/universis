import { Component, OnInit } from '@angular/core';
import {defineLocale, BsLocaleService} from 'ngx-bootstrap';
import {el} from './config/config';
@Component({
  selector: 'app-lib-datepicker',
  templateUrl: './datepicker.component.html',
  styles: []
})
export class DatepickerComponent implements OnInit {
  bsValue: Date = new Date();
  constructor(private _localeService: BsLocaleService) {
  }

  ngOnInit() {
    defineLocale('el', el);
    this._localeService.use('el');
  }

}
