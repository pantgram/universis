import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CardsComponent} from './components/cards-component/cards.component';
import {ExpandableCardsComponent} from './components/expandable-cards/expandable-cards.component';
import {ProfileCardComponent} from './components/profile-card/profile-card.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'cards'
  },
  {
    path: 'cards',
    component: CardsComponent,
    data: {
      title: 'Cards'
    }
  },
  {
    path: 'expandable-cards',
    component: ExpandableCardsComponent,
    data: {
      title: 'Expandable Cards'
    }
  },
  {
    path: 'profile-cards',
    component: ProfileCardComponent,
    data: {
      title: 'Profile Cards'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CardsRoutingModule {
}
